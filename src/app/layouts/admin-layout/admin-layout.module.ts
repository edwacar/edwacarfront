import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { VehiculosComponent }       from '../../pages/vehiculos/vehiculos.component';
import { UserComponent }            from '../../pages/user/user.component';
import { ClientesComponent }           from '../../pages/clientes/clientes.component';
import { TypographyComponent }      from '../../pages/typography/typography.component';
import { ServiciosComponent }           from '../../pages/servicios/servicios.component';
import { MapsComponent }            from '../../pages/maps/maps.component';
import { NotificationsComponent }   from '../../pages/notifications/notifications.component';
import { UpgradeComponent }         from '../../pages/upgrade/upgrade.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    NgbModule
  ],
  declarations: [
    VehiculosComponent,
    UserComponent,
    ClientesComponent,
    UpgradeComponent,
    TypographyComponent,
    ServiciosComponent,
    MapsComponent,
    NotificationsComponent,
  ]
})

export class AdminLayoutModule {}
