import { Routes } from '@angular/router';

import { VehiculosComponent } from '../../pages/vehiculos/vehiculos.component';
import { UserComponent } from '../../pages/user/user.component';
import { ClientesComponent } from '../../pages/clientes/clientes.component';
import { TypographyComponent } from '../../pages/typography/typography.component';
import { ServiciosComponent } from '../../pages/servicios/servicios.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { NotificationsComponent } from '../../pages/notifications/notifications.component';
import { UpgradeComponent } from '../../pages/upgrade/upgrade.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'vehiculos',      component: VehiculosComponent },
    { path: 'user',           component: UserComponent },
    { path: 'clientes',          component: ClientesComponent },
    { path: 'typography',     component: TypographyComponent },
    { path: 'servicios',          component: ServiciosComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'notifications',  component: NotificationsComponent },
    { path: 'upgrade',        component: UpgradeComponent }
];
